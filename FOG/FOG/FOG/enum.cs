﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FOG {
    public enum GameState {
        titlescreen, ingame, howtoplay
    }

    public enum EnemyType {
        white, gold, red, blue, green
    }

    public enum WaveState {
        ONE,TWO,THREE,FOUR
    }

    public enum LEFTRIGHT { 
        rightmove, leftmove 
    }
}
