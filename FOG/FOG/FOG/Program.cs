using System;

namespace FOG {
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (FOG game = new FOG())
            {
                game.Run();
            }
        }
    }
#endif
}

