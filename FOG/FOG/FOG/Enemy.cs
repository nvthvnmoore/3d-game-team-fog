﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FOG {
    class Enemy {
        public Model model { get; protected set; }
        protected Matrix world = Matrix.Identity;
        protected Matrix rotation = Matrix.Identity;
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();

        // Unit Stats
        public EnemyType TYPE;
        public bool ALIVE;
        public float SPEED;

        public Vector3 colorid;
        public Vector3 postion;

        LEFTRIGHT sidemove;
        private bool moveup;
        private float bounce_rad;

        // White orb properties
        public bool fade = false; public float fadetimer = 10f; float fadealpha = 1f;

        public Enemy(EnemyType type) {
            model = Assets.getModel("SPHERE");
            TYPE = type;
            ALIVE = true;

            if(TYPE == EnemyType.white) {
                colorid = new Vector3(1f, 1f, 1f);
                fadealpha = 0.2f;
                SPEED = .025f;
            }
            else if(TYPE == EnemyType.gold) {
                colorid = new Vector3(.95f, .75f, .30f);
                SPEED = .4f;
            }
            else if(TYPE == EnemyType.red) {
                colorid = new Vector3(1f, .3f, .3f);
                SPEED = .025f;
            }
            else if(TYPE == EnemyType.blue) {
                colorid = new Vector3(.25f, .6f, 1f);
                SPEED = .025f;
            }
            else if(TYPE == EnemyType.green) {
                colorid = new Vector3(.5f, 1f, .5f);
                SPEED = .025f;
            }
            postion = new Vector3(RandomNum(-65, 65), RandomNum(7, 20), RandomNum(-40, 0));
            //postion = new Vector3(0,0,-25);
            sidemove = RandomSideMove();
        }

        public virtual void UpdateUnit() {
            AutoMoveUnit(); // CALLS METHOD TO AUTO MOVE UNIT.
        }

        public void DrawUnit(Camera camera) {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
            
            rotation *= Matrix.CreateRotationY(MathHelper.Pi / 180); // Rotates the object.
                foreach(ModelMesh mesh in model.Meshes) {
                    foreach(BasicEffect effect in mesh.Effects) {
                        effect.EnableDefaultLighting();
                        effect.Projection = camera.projection;
                        effect.View = camera.view;
                        effect.DiffuseColor = colorid;
                        effect.World = (world * rotation) * mesh.ParentBone.Transform * Matrix.CreateTranslation(postion);
                        effect.Alpha = fadealpha;
                    }
                    mesh.Draw();
                }
   
        }

        public LEFTRIGHT RandomSideMove() { // Randomly generate a movement (left or right) for spawned units.
            Array values = Enum.GetValues(typeof(LEFTRIGHT));
            LEFTRIGHT randomside = (LEFTRIGHT) values.GetValue(random.Next(values.Length));
            return randomside;
        }

        private void AutoMoveUnit() {
            // Move units LEFT/RIGHT.
            if(postion.X >= 50) {
                sidemove = LEFTRIGHT.leftmove;
            }
            else if(postion.X <= -50) {
                sidemove = LEFTRIGHT.rightmove;
            }

            if(sidemove == LEFTRIGHT.rightmove) {
                postion += new Vector3(SPEED, bounce_rad, bounce_rad * 0.75f);
            }
            else if(sidemove == LEFTRIGHT.leftmove) {
                postion -= new Vector3(SPEED, bounce_rad, bounce_rad * 0.75f);
            }

            // Move units UP/DOWN/FORWARDS/BACKWARDS.
            if(bounce_rad >= 0.2f) {
                moveup = false;
            }
            else if(bounce_rad <= -0.2f) {
                moveup = true;
            }

            if(moveup) {
                bounce_rad += .01f;
            }
            else if(!moveup) {
                bounce_rad -= .01f;
            }
        }

        public static int RandomNum(int min, int max) {
            lock(syncLock) {
                return random.Next(min, max);
            }
        }
    }
}