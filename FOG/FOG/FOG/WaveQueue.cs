﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FOG {
    class WaveQueue {
        static Random random = new Random();

        public static WaveQueue waveque;
        public Queue<Enemy> waveone;

        public static WaveQueue Instance {
            get {
                if(waveque == null) {
                    waveque = new WaveQueue();
                    waveque.waveone = new Queue<Enemy>();
                }
                return waveque;
            }
        }

        public static Enemy getNextWaveUnit(WaveState wave) {
            
                return Instance.waveone.Dequeue();
           
        }

        public static int getWaveSize(WaveState wave) {
            if(wave == WaveState.ONE) {
                return Instance.waveone.Count;
            }
            return 0;
        }

        public static void setWaveOne() {
            for(int i = 0; i < 100; i++) {
                Instance.waveone.Enqueue(new Enemy(RandomEnemyType()));
            }
        }


        public static EnemyType RandomEnemyType() { // Generates a random EnemyType.
            Array values = Enum.GetValues(typeof(EnemyType));
            EnemyType randomunit = (EnemyType)values.GetValue(random.Next(values.Length));
            return randomunit;
        }

    }
}
