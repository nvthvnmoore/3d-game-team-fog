﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FOG {
    public class FOG : Microsoft.Xna.Framework.Game {
        const int DefaultScreenWidth = 1280, DefaultScreenHeight = 800; // Fixed screen size.
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        BasicEffect effect;

        // Classes/Enums
        public Camera camera;               // Creates camera class to allow player to tilt the screen.
        SpawnManager spawnmanager;          // Creates the Spawn Manager class to help spawn units.
        GameState currentscreen;            // Enum for the current display screen.
        Environment environment;

        // Title Variables
        float titlefog = 0; bool fogup = false;         // Helps controls the title screen fog.

        // Ingame Variables
        bool ingamesetup = false;                       // Checks to see if the first intial setup has been done.
        float ingamefog = 0, fogtimer = 0; bool ingamefogup = false;  // Helps controls the ingame screen fog.
        
        // Environment Variables
        VertexPositionTexture[] verts;
        VertexBuffer vertexBuffer;
        Texture2D grasstexture;
        Model weed;
        Model gun;
        Model[] tree;    

        public FOG() {
            graphics = new Microsoft.Xna.Framework.GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = DefaultScreenWidth; // Set fixed width.
            graphics.PreferredBackBufferHeight = DefaultScreenHeight; // Set fixed height.
            this.IsMouseVisible = true; // Allows mouse cursor to be shown. 
            Content.RootDirectory = "Content";
        }

        protected override void Initialize() {
            currentscreen = GameState.titlescreen;

            camera = new Camera(this, new Vector3(0, 0, 10), Vector3.Zero, Vector3.Up);
            Components.Add(camera);

            spawnmanager = new SpawnManager(this, WaveState.ONE);
            effect = new BasicEffect(GraphicsDevice);

            environment = new Environment(GraphicsDevice, Content, camera, "skybox2");
            tree = new Model[4];
            verts = new VertexPositionTexture[4];
            verts[0] = new VertexPositionTexture(new Vector3(100, -1, -100), new Vector2(0, 1));
            verts[1] = new VertexPositionTexture(new Vector3(100, -1, 100), new Vector2(0, 0));
            verts[2] = new VertexPositionTexture(new Vector3(-100, -1, -100), new Vector2(1, 1));
            verts[3] = new VertexPositionTexture(new Vector3(-100, -1, 100), new Vector2(1, 0));
            base.Initialize();
        }

        protected override void LoadContent() {
            // Models
            Assets.addModel("SPHERE", Content.Load<Model>("TitleAssets/sphere"));
            tree[0] = Content.Load<Model>(@"IngameAssets\t3");
            tree[1] = Content.Load<Model>(@"IngameAssets\t2");
            tree[2] = Content.Load<Model>(@"IngameAssets\t4");
            tree[3] = Content.Load<Model>(@"IngameAssets\tree5");
            weed = Content.Load<Model>(@"IngameAssets\weed1");
            gun = Content.Load<Model>(@"IngameAssets\Gun");

            // Sprites
            Assets.addTexture("SCREENFOG", Content.Load<Texture2D>("IngameAssets/fog_white"));
            Assets.addTexture("SCOPE", Content.Load<Texture2D>("IngameAssets/gunscope"));
            grasstexture = Content.Load<Texture2D>(@"IngameAssets\grass");

            // Other
            vertexBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionTexture), verts.Length, BufferUsage.None);

            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent() { }

        protected override void Update(GameTime gameTime) {
            if(currentscreen == GameState.titlescreen) {
                if(new Rectangle(510, 500, 237, 71).Contains(Mouse.GetState().X, Mouse.GetState().Y)
                 && Mouse.GetState().LeftButton == ButtonState.Pressed) {
                    currentscreen = GameState.ingame;
                }
                else if(new Rectangle(500, 600, 260, 67).Contains(Mouse.GetState().X, Mouse.GetState().Y)
                    && Mouse.GetState().LeftButton == ButtonState.Pressed) {
                    currentscreen = GameState.howtoplay;
                }
            }
            // In-game Update().
            else {
                this.IsMouseVisible = false;

                if(!ingamesetup) { // In-game startup intialization.
                    Mouse.SetPosition(GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width/2,
                        GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height/2);
                    Components.Add(spawnmanager);
                    ingamesetup = true;
                }
            }
            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Escape))
                this.Exit();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.DodgerBlue);
            spriteBatch.Begin();

            // Title Screen.
            if(currentscreen == GameState.titlescreen) {
                spriteBatch.Draw(Assets.getTexture("SCREENFOG"), new Vector2(0, 0), Color.White * titlefog);
                spriteBatch.Draw(Content.Load<Texture2D>("TitleAssets/FOG"), new Vector2(420, 150), Color.White);
                spriteBatch.Draw(Content.Load<Texture2D>("TitleAssets/START_GAME"), new Vector2(510, 500), Color.White);
                spriteBatch.Draw(Content.Load<Texture2D>("TitleAssets/HOW_TO_PLAY"), new Vector2(500, 600), Color.White);

                if(new Rectangle(510, 500, 237, 71).Contains(Mouse.GetState().X, Mouse.GetState().Y)) {
                    spriteBatch.Draw(Content.Load<Texture2D>("TitleAssets/START_GAME"), new Vector2(510, 500), Color.Yellow);
                }
                else if(new Rectangle(500, 600, 260, 67).Contains(Mouse.GetState().X, Mouse.GetState().Y)) {
                    spriteBatch.Draw(Content.Load<Texture2D>("TitleAssets/HOW_TO_PLAY"), new Vector2(500, 600), Color.Yellow);
                }

                // Fade fog in and out.
                if(titlefog >= 0.65f) { fogup = false; }
                else if(titlefog <= 0.15f) { fogup = true; }

                if(fogup) { titlefog = titlefog + 0.0025f; }
                else if(!fogup) { titlefog = titlefog - 0.0025f; }
            }
            // In-game Draw().
            else {
                spriteBatch.Draw(Assets.getTexture("SCREENFOG"), new Vector2(0,0),Color.LightGray * ingamefog);
                // Increment the screen fog.
                fogtimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if(fogtimer >= 1f) { // Increment fog by 
                    ingamefog += 1f / (float)spawnmanager.wavetime;
                    fogtimer = 0;
                }
                spriteBatch.Draw(Assets.getTexture("SCOPE"), new Vector2(512,272),Color.White);

                // UI Texts
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts/Wave_Display"),
                    "FOG AMOUNT: " + ((int)(ingamefog * 100)).ToString() + "%", new Vector2(1090, 700), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts/Wave_Display"), 
                     "TIME REMAINING: " + (spawnmanager.wavetime-(ingamefog * spawnmanager.wavetime)).ToString() + " SECS"
                    , new Vector2(970,750), Color.White);

                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts/Wave_Display"),
                   "ORBS REMAINING: " + (spawnmanager.totalcnt-spawnmanager.whitecnt).ToString(), 
                   new Vector2(1040, 50), Color.Yellow);
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts/Wave_Display"),
                    "GOLD ORB: " + spawnmanager.goldcnt.ToString() , new Vector2(1112, 90), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts/Wave_Display"),
                    "RED ORB: " + spawnmanager.redcnt.ToString(), new Vector2(1124, 120), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts/Wave_Display"),
                    "BLUE ORB: " + spawnmanager.bluecnt.ToString(), new Vector2(1113, 150), Color.White);
                spriteBatch.DrawString(Content.Load<SpriteFont>("Fonts/Wave_Display"),
                    "GREEN ORB: " + spawnmanager.greencnt.ToString(), new Vector2(1101, 180), Color.White);

                // Environment Draws
                environment.DrawGround(effect, verts, grasstexture, vertexBuffer);
                environment.DrawSkyBox(spriteBatch);
                environment.DrawTrees(tree);
                environment.DrawWeeds(weed);
                //environment.DrawGun(gun);
                spawnmanager.DrawSpawns(gameTime);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }


    }
}

