using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace FOG {
    public class SpawnManager : DrawableGameComponent {
        Random random = new Random();

        // Date Structures
        List<Enemy> UNITLIST = new List<Enemy>(); // Contains the current list of units.
        WaveQueue wavelist = new WaveQueue();
        
        private float spawntimer = 0; // Leave intialized at 0.
        Model spmodel;

        // Wave Properties
        public WaveState wave;       // The current wave number.
        public int wavetime;         // Max minutes per wave.
        private float spawnspeed;    // Incremented in seconds.

        public bool END_LEVEL;
         
        public int whitecnt = 0, goldcnt = 0, redcnt = 0, bluecnt = 0, greencnt = 0, totalcnt = 0;

        public SpawnManager(Game game, WaveState wave) : base(game) {
            this.wave = wave;
            END_LEVEL = false;
            if(this.wave == WaveState.ONE) {
                wavetime = 300;     // time in seconds.
                spawnspeed = 0.25f; // Time between spawn.
            }
        }

        public override void Initialize() {
            spmodel = Game.Content.Load<Model>("TitleAssets/sphere");

            base.Initialize();
        }

        protected override void LoadContent() {
            WaveQueue.setWaveOne();
            base.LoadContent(); 
        }

        public override void Update(GameTime gameTime) {
            // Spawn Timer
            spawntimer += (float) gameTime.ElapsedGameTime.TotalSeconds;
            if(WaveQueue.getWaveSize(wave) != 0 && spawntimer >= spawnspeed) { // Add the unit into the list.
                Enemy tmp = WaveQueue.getNextWaveUnit(wave);
                UNITLIST.Add(tmp); // ADDS A RANDOM UNIT TO THE LIST.
                HeadCount(tmp.TYPE, +1);
                spawntimer = 0;
            }

            for(int i = 0; i < UNITLIST.Count; i++) { // Loop through all models and call Update.
                if(UNITLIST[i].ALIVE) {
                    /*if(UNITLIST[i].TYPE == EnemyType.white) {
                        UNITLIST[i].fadetimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                        if(UNITLIST[i].fadetimer >= 10f) {
                            UNITLIST[i].fade = !UNITLIST[i].fade;
                            UNITLIST[i].fadetimer = 0f;
                        }
                    }*/
                    UNITLIST[i].UpdateUnit();

                }
                else {
                    UNITLIST.Remove(UNITLIST[i]);
                    HeadCount(UNITLIST[i].TYPE, -1);
                }
            }
            base.Update(gameTime);
        }

        public void DrawSpawns(GameTime gameTime) { // Draws all ALIVE enemies to the screen.
            // Loop through and draw each model
            foreach(Enemy unit in UNITLIST) {
                
                unit.DrawUnit(((FOG)Game).camera);
                

                if(unit.ALIVE && unit.TYPE != EnemyType.white) { // Checks to see if end of level.
                    END_LEVEL = false;
                }
                else {
                    END_LEVEL = true;
                }
            }
            base.Draw(gameTime);
        }

        public EnemyType RandomEnemyType() { // Generates a random EnemyType.
            Array values = Enum.GetValues(typeof(EnemyType));
            EnemyType randomunit = (EnemyType)values.GetValue(random.Next(values.Length));
            return randomunit;
        }

        private void HeadCount(EnemyType type, int num) {
            if(type == EnemyType.white) {
                whitecnt = whitecnt + num;
            }
            else if(type == EnemyType.gold) {
                goldcnt = goldcnt + num;
            }
            else if(type == EnemyType.red) {
                redcnt = redcnt + num;
            }
            else if(type == EnemyType.blue) {
                bluecnt = bluecnt + num;
            }
            else if(type == EnemyType.green) {
                greencnt = greencnt + num; ;
            }
            totalcnt = totalcnt + num;
            //totalcnt = UNITLIST.Count
        }

    }
}
