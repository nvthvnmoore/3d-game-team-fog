﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace FOG {
    class XShot {
        Matrix rotation = Matrix.Identity;
        protected Matrix world = Matrix.Identity;

        // Rotation and movement variables
        public Model model;
        float yawAngle = 0;
        float pitchAngle = 0;
        float rollAngle = 0;
        Vector3 direction;
        public Vector3 position;

        public XShot(Model m, Vector3 Position,
            Vector3 Direction, float yaw, float pitch, float roll) {
            model = m;
            world = Matrix.CreateTranslation(Position);
            yawAngle = yaw;
            pitchAngle = pitch;
            rollAngle = roll;
            direction = Direction;
        }

        public void Update() {
            // Rotate model
            rotation *= Matrix.CreateFromYawPitchRoll(yawAngle,
                pitchAngle, rollAngle);

            // Move model
            world *= Matrix.CreateTranslation(direction);
        }

        public Matrix GetWorld() {
            return rotation * world;
        }

        public void Draw(Camera camera) {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach(ModelMesh mesh in model.Meshes) {
                foreach(BasicEffect be in mesh.Effects) {
                    be.EnableDefaultLighting();
                    be.DiffuseColor = new Vector3(.075f, .075f, .075f);
                    be.Projection = camera.projection;
                    be.View = camera.view;
                    be.World = GetWorld() ;
                }

                mesh.Draw();
            }
        }

        public bool CollidesWith(Model otherModel, Matrix otherWorld) {
            // Loop through each ModelMesh in both objects and compare
            // all bounding spheres for collisions
            foreach(ModelMesh myModelMeshes in model.Meshes) {
                foreach(ModelMesh hisModelMeshes in otherModel.Meshes) {
                    if(myModelMeshes.BoundingSphere.Transform(
                        GetWorld()).Intersects(
                        hisModelMeshes.BoundingSphere.Transform(otherWorld)))
                        return true;
                }
            }
            return false;
        }
    }
}

