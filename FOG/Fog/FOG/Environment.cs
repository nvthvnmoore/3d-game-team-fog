﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace FOG
{

    class Environment
    {
        static Random random = new Random();
        public const int environmentWidth = 20;
        public const int environmentHeight = 20;
        private GraphicsDevice device;
        private Camera camera;
        private Model skyboxModel;
        private int[] randomXCoord1;
        private int[] randomZCoord1;
        private int[] randomDegree1;
        private int[] randomXCoord2;
        private int[] randomZCoord2;
        private int[] randomDegree2;
        private int[] randomXCoord3;
        private int[] randomZCoord3;
        private int[] randomDegree3;
        private int[] randomTreeIndex;
        private int[] randomXCoord4;
        private int[] randomZCoord4;
        private int farLine = 100;
        private int midLine = 90;
        private int closeLine = 80;
        private int numberOfTrees = 50;
        private int numberOfWeeds = 500;

        public Environment(GraphicsDevice device, ContentManager Content, Camera camera, string skyboxName)
        {
            this.device = device;
            this.camera = camera;
            skyboxModel = Content.Load<Model>(@"IngameAssets\skybox2");
            randomXCoord1 = new int[numberOfTrees];
            randomZCoord1 = new int[numberOfTrees];
            randomDegree1 = new int[numberOfTrees];
            randomXCoord2 = new int[numberOfTrees];
            randomZCoord2 = new int[numberOfTrees];
            randomDegree2 = new int[numberOfTrees];
            randomXCoord3 = new int[numberOfTrees];
            randomZCoord3 = new int[numberOfTrees];
            randomDegree3 = new int[numberOfTrees];
            randomTreeIndex = new int[numberOfTrees];
            randomXCoord4 = new int[numberOfWeeds];
            randomZCoord4 = new int[numberOfWeeds];
            RandomIndexes();
            RandomDegree();
            RandomX();
            RandomZ();

        }

        public void DrawSkyBox(SpriteBatch spriteBatch)
        {
            spriteBatch.GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            foreach (ModelMesh mesh in skyboxModel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.LightingEnabled = false;
                    effect.World = Matrix.CreateTranslation(camera.cameraPosition);
                    effect.View = camera.view;
                    effect.Projection = camera.projection;

                }
                mesh.Draw();
            }
        }


        public void DrawGround(BasicEffect effect, VertexPositionTexture[] verts, Texture2D texture, VertexBuffer vBuffer)
        {
            effect.Texture = texture;
            effect.TextureEnabled = true;
            effect.World = Matrix.Identity;
            effect.View = camera.view;
            effect.Projection = camera.projection;
            effect.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.SetVertexBuffer(vBuffer);
                device.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleStrip, verts, 0, 2);
            }
        }

        public void DrawWeeds(Model weed)
        {
            for (int i = 0; i < numberOfWeeds; i++)
            {
                Matrix[] transforms = new Matrix[weed.Bones.Count];
                weed.CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in weed.Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                        effect.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                        effect.Projection = camera.projection;
                        effect.View = camera.view;
                        effect.World = Matrix.CreateTranslation(randomXCoord4[i], -1.11f, randomZCoord4[i]);
                    }
                    mesh.Draw();
                }
            }
        }

        /*public void DrawGun(Model gun)
        {
            Matrix[] transforms = new Matrix[gun.Bones.Count];
            gun.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in gun.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                    effect.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                    effect.Projection = camera.Projection;
                    effect.View = camera.View;
                    effect.World = Matrix.CreateTranslation(camera.position.X, 0.5f, camera.position.Z) *
                        Matrix.CreateRotationY(camera.rotation);
                }
                mesh.Draw();
            }
        }*/

        public void DrawTrees(Model[] tree)
        {
            for (int i = 0; i < numberOfTrees; i++)
            {
                Matrix[] transforms = new Matrix[tree[randomTreeIndex[i]].Bones.Count];
                tree[randomTreeIndex[i]].CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in tree[randomTreeIndex[i]].Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                        effect.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                        effect.Projection = camera.projection;
                        effect.View = camera.view;
                        effect.World = Matrix.CreateTranslation(randomXCoord1[i], -1, randomZCoord1[i]+10);
                    }
                    mesh.Draw();
                }
            }

            for (int i = 0; i < numberOfTrees; i++)
            {
                Matrix[] transforms = new Matrix[tree[randomTreeIndex[i]].Bones.Count];
                tree[randomTreeIndex[i]].CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in tree[randomTreeIndex[i]].Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                        effect.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                        effect.Projection = camera.projection;
                        effect.View = camera.view;
                        effect.World = Matrix.CreateTranslation(randomXCoord2[i], -1, randomZCoord2[i]+10);
                    }
                    mesh.Draw();
                }
            }

            for (int i = 0; i < numberOfTrees; i++)
            {
                Matrix[] transforms = new Matrix[tree[randomTreeIndex[i]].Bones.Count];
                tree[randomTreeIndex[i]].CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in tree[randomTreeIndex[i]].Meshes)
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        effect.EnableDefaultLighting();
                        effect.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                        effect.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                        effect.Projection = camera.projection;
                        effect.View = camera.view;
                        effect.World = Matrix.CreateTranslation(randomXCoord3[i], -1, randomZCoord3[i]+10);
                    }
                    mesh.Draw();
                }
            }
        }

        private void RandomX()
        {
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomXCoord1[i] = (int)(farLine * Math.Cos(randomDegree1[i] * Math.PI / 180F));
            }
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomXCoord2[i] = (int)(midLine * Math.Cos(randomDegree2[i] * Math.PI / 180F));
            }
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomXCoord3[i] = (int)(closeLine * Math.Cos(randomDegree3[i] * Math.PI / 180F));
            }
            for (int i = 0; i < numberOfWeeds; i++)
            {
                randomXCoord4[i] = (int)(random.Next(-100, 100));
            }
        }
        private void RandomZ()
        {
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomZCoord1[i] = (int)(farLine * Math.Sin(randomDegree1[i] * Math.PI / 180F));
            }
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomZCoord2[i] = (int)(midLine * Math.Sin(randomDegree2[i] * Math.PI / 180F));
            }
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomZCoord3[i] = (int)(closeLine * Math.Sin(randomDegree3[i] * Math.PI / 180F));
            }
            for (int i = 0; i < numberOfWeeds; i++)
            {
                randomZCoord4[i] = (int)(random.Next(-100, 30));
            }
        }
        private void RandomDegree()
        {
            Random random1 = new Random();
            Random random2 = new Random();
            Random random3 = new Random();
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomDegree1[i] = (int)random1.Next(0, 360);
            }
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomDegree2[i] = (int)random1.Next(0, 360);
            }
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomDegree3[i] = (int)random1.Next(0, 360);
            }
        }

        private void RandomIndexes()
        {
            Random random1 = new Random();
            for (int i = 0; i < numberOfTrees; i++)
            {
                randomTreeIndex[i] = (int)random1.Next(0, 3);
            }
        }
    }
}
